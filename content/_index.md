---
title: "Página inicial"
date: 2021-10-13T23:28:40-03:00
draft: false
---

<h1>Bem-vindos e bem-vindas ao RECEITOPS!</h1>

<p>Aqui você encontrará algumas receitas deciciosas!</p>

<p>Você pode buscar por:</p>

<ul>
    <li>Receitas salgadas</li>
    <li>Receitas doces</li>
    <li>Bebidas</li>
</ul>
