---
title: "Batidinha de Frutas"
date: 2021-10-13T23:27:57-03:00
draft: false
photo: "https://img.itdg.com.br/tdg/images/recipes/000/028/239/327648/327648_original.jpg"
section: "Bebidas"
---

<article>
<h4>Ingredientes:</h4>

1. 01 xícara de cachaça, rum ou vodka;
2. 02 xícaras da polpa da fruta (abacaxi, morango, maracujá, ...);
3. 02 xícaras de gelo picado;
4. 01 lata de leite condensado.
</article>

<article>
<h4>Modo de preparo:</h4>

1. Bata todos os ingredientes no liquidificador;
2. Sirva e decore as taças para dar um toque especial.
</article>