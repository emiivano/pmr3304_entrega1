---
title: "Cappuccino"
date: 2021-10-13T23:27:57-03:00
draft: false
photo: "https://img.itdg.com.br/tdg/images/recipes/000/000/756/131263/131263_original.jpg"
section: "Bebidas"
---

<article>
<h4>Ingredientes:</h4>

1. 50g de café solúvel;
2. 250g de leite em pó (integral ou desnatado);
3. 03 colheres (sopa) de chocolate em pó;
4. 01 colher (chá) de bicarbonato de sódio;
5. 01 colher (ché) de canela em pó;
6. 250g de açúcar.
</article>

<article>
<h4>Modo de preparo:</h4>

1. Bata o café no liquidificador até ficar fino;
2. Junte todos os ingredientes em uma tigela;
3. Peneire e guarde;
4. Use 02 colheres de sobremesa do pó para 01 xícara da água fervente.
</article>