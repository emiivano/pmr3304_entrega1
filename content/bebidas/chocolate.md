---
title: "Chocolate Quente"
date: 2021-10-13T23:27:57-03:00
draft: false
photo: "https://img.itdg.com.br/tdg/images/recipes/000/130/871/321194/321194_original.jpg"
section: "Bebidas"
---

<article>
<h4>Ingredientes:</h4>

1. 02 xícaras (chá) de leite;
2. 01 colher (sopa) de amido de milho;
3. 03 colheres (sopa) de chocolate em pó;
4. 04 colheres (sopa) de açúcar;
5. 01 canela em pau;
6. 01 caixa de creme de leite.
</article>

<article>
<h4>Modo de preparo:</h4>

1. Bata o leite, o amido de milho, o chocolate em pó e o açúcar em um liquidificador;
2. Despeje a mistura em uma panela com a canela e leve ao fogo baixo, mexendo sempre até ferver;
3. Desligue, adicione o creme de leite e mexa bem até obter uma mistura homogênea;
4. Retire a canela e sirva quente.

Obs: você pode finalizar seu chocolate incrementando chantilly, raspas de chocolate e canela em pó!
</article>