---
title: "Bolo de Sorvete"
date: 2021-10-13T23:27:57-03:00
draft: false
photo: "https://img.itdg.com.br/tdg/images/recipes/000/121/136/293013/293013_original.jpg"
section: "Doces"
---

<article>
<h4>Ingredientes para a calda:</h4>

1. 05 colheres de chocolate em pó;
2. 07 colheres de água.

<h4>Ingredientes para o primeiro creme:</h4>

1. 04 gemas;
2. 01 lata de leite condensado;
3. 01 lata de leite;

<h4>Ingredientes para o segundo creme:</h4>

1. 04 claras;
2. 01 lata de creme de leite;
3. 05 colheres de açúcar.
</article>

<article>
<h4>Modo de preparo:</h4>

1. Coloque em uma panela os ingredientes da calda e misture até dissolver o chocolate e engrossar;
2. Coloque a calda em uma forma, untando somente o fundo;
3. Reserve a calda no congelador;
4. Coloque os ingredientes do primeiro creme no liquidificador e bata até misturar bem;
5. Coloque a mistura na panela, leve ao fogo e misture até engrossar;
6. Coloque a panela em uma bandeja com água fria para esfriar o creme;
7. Coloque a clara dp segundo creme na batedeira e bata até virar neve;
8. Adicione os outros ingredientes do segundo creme e bata novamente até misturar bem;
9. Adicione o primeiro creme e bata novamente para misturar;
10. Retire a forma da calda do freezer e coloque a mistura;
11. Leve ao freezer por 24 horas até virar um sorvete;
12. Desenforme o bolo e sirva.

Obs: Se desejar fazer o bolo de outro sabor, coloque um suco de fruto ao invés do leite.
</article>