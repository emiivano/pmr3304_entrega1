---
title: "Brigadeiro"
date: 2021-10-13T23:27:57-03:00
draft: false
photo: "https://img.itdg.com.br/tdg/images/recipes/000/000/114/320431/320431_original.jpg"
section: "Doces"
---

<article>
<h4>Ingredientes:</h4>

1. 01 colher (sopa) de margarina sem sal;
2. 01 caixa de leite condensado;
3. 07 colheres (sopa) de achocolatado ou 04 colheres (sopa) de chocolate em pó;
4. Chocolate granulado.
</article>

<article>
<h4>Modo de preparo:</h4>

1. Em uma panela funda, acrescente o leite condensado, a margarina e o chocolate em pó;
2. Cozinhe em fogo médio e mexa até que o brigadeiro comece a desgrudar da panela;
3. Deixe esfriar e depois faça pequenas bolas;
4. Por fim, passe no chocolate granulado para decorar.

Obs: Para saber se o brigadeiro está no ponto certo, passe a colher no meio da panela, se a mistura demorar a voltar para o meio e você conseguiu enxergar o fundo da panela, o brigadeiro está pronto.
</article>