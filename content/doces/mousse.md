---
title: "Mousse de Limão"
date: 2021-10-13T23:27:57-03:00
draft: false
photo: "https://img.itdg.com.br/tdg/images/recipes/000/155/693/332853/332853_original.jpg"
section: "Doces"
---

<article>
<h4>Ingredientes:</h4>

1. 01 lata de leite condensado;
2. 01 lata de creme de leite;
3. 1/2 xícara de suco de limão.
</article>

<article>
<h4>Modo de preparo:</h4>

1. Bata o creme de leite e o leite condensado no liquidificador;
2. Vá acrescentando aos poucos o suco de limão;
3. Quando a mistura ficar bem consistente, leve à geladeira.

Obs: você ainda pode acrescentar pedaços de chocolate, nozes, biscoito triturado ou o que quiser!
</article>