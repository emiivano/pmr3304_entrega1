---
title: "Pavê de Chocolate"
date: 2021-10-13T23:27:57-03:00
draft: false
photo: "https://img.itdg.com.br/tdg/images/recipes/000/000/581/349098/349098_original.jpg"
section: "Doces"
---

<article>
<h4>Ingredientes:</h4>

1. 01 pacote de bolacha maisena;
2. 01 lata de leite condensado;
3. 1 lata de creme de leite;
4. 01 lata de leite;
5. 2 gemas;
6. 01 colher (sopa) de farinha de trigo;
7. 02 xícaras (chá) de morango;
8. 01 colher (chá) de baunilha;
9. 100g de chocolate meio amargo;
10. 01 xícara (chá) de chantilly;
11. Morango e chantilly para decorar.
</article>

<article>
<h4>Modo de preparo:</h4>

1. Misture numa panela o leite condensado, o leite, as gemas levemente batidas, a baunilha e a farinha;
2. Mexa até engrossar;
3. Desligue e junte o creme de leite com o soro;
4. Divida o creme igualmente em dois refratários;
5. Em um dos refratários junte o chocolate raspado e 01 xícara (chá) de morangos picados, misture até derreter o chocolate e reserve;
6. No outro refratário junte o chantilly e 01 xícara (chá) de morangos picados, misture e reserve;
7. Monte o pavê na seguite ordem: o creme com chantilly, as bolachas, o creme de chocolate, mais bolachas, o restante do creme com chantilly e o restante do creme de chocolate;
8. Por fim, decore o pavê com chantilly e morangos.
</article>