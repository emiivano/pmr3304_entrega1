---
title: "Pudim"
date: 2021-10-13T23:27:57-03:00
draft: false
photo: "https://img.itdg.com.br/tdg/images/recipes/000/031/593/318825/318825_original.jpg"
section: "Doces"
---

<article>
<h4>Ingredientes para a massa:</h4>

1. 01 lata de leite condensado;
2. 01 xícara de leite;
3. 4 ovos.

<h4>Ingredientes para a calda:</h4>

1. 01 xícara (chá) de açúcar;
2. 1/3 de xícara (chá) de água.
</article>

<article>
<h4>Modo de preparo:</h4>

1. Adicione os ingredientes da calda em uma panela;
2. Misture até formar uma calda;
3. Unte uma forma com a calda e reserve;
4. Bata todos os ingredientes da massa no liquidificador e despeje na forma caramelizada;
5. Leve para assar em banho-maria por 40 minutos;
7. Desenforme e sirva.
</article>