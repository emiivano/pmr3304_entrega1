---
title: "Dadinho de Tapioca"
date: 2021-10-13T23:28:40-03:00
draft: false
photo: "https://img.itdg.com.br/tdg/images/recipes/000/165/944/304385/304385_original.jpg"
section: "Salgados"
---

<article>
<h4>Ingredientes:</h4>

1. 500g de farinha de tapioca granulada
2. 1 litro de leite líquido integral
3. 500g de queijo coalho ralado
4. Sal a gosto
5. Pimenta-do-reino a gosto 
6. 01 pote de geleia agridoce de pimenta
</article>

<article>
<h4>Modo de preparo:</h4>

1. Aqueça o leite em uma panela;
2. Desligue o fogo, adicione o queijo coalho ralado e continue mexendo; 
3. Adicione a farinha de tapioca granulada e continue mexendo;
4. Adicione o sal e pimenta;
5. Quando todos os ingredientes estiverem bem misturados, despeje em uma forma coberta com papel filme; 
6. Cubra em cima e leve para a geladeira por duas horas;
7. Retire da geladeira, corte em quadradinhos e frite no óleo ou coloque no forno até dourar;
8. Sirva acomapnhado da geleia de pimenta.
</article>