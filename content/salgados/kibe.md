---
title: "Kibe de Forno"
date: 2021-10-13T23:28:40-03:00
draft: false
photo: "https://img.itdg.com.br/tdg/images/recipes/000/019/882/354039/354039_original.jpg"
section: "Salgados"
---

<article>
<h4>Ingredientes:</h4>

1. 01 xícara (chá) de trigo para kibe
2. 01 xícara (chá) de coxão duro moído
3. 01 cebola (média)
4. 02 dentes de alho amassados
5. 1/2 xícara (chá) hortelã 
6. Suco de 01 limão
7. 01 colher (chá) de sal
8. Azeite a gosto
</article>

<article>
<h4>Modo de preparo:</h4>

1. Deixe o trigo de molho na água por uma hora (para a desidratação do trigo);
2. Esprema bem em uma peneira ou pano;
3. Misture todos os ingredientes em um recipiente;
4. Coloque em um refratário ou assadeira untada com óleo;
5. Corte em cubos na diagonal e regue com bastante azeite; 
6. Leve ao forno médio (180 graus) por uns 30 a 40 minutos.

Obs: Se preferir o kibe recheado, coloque metade da massa, acrescente o recheio e por fim cubra com a outra metade da massa.
</article>