---
title: "Salada de Macarrão"
date: 2021-10-13T23:27:57-03:00
draft: false
photo: "https://img.itdg.com.br/tdg/images/recipes/000/061/580/328762/328762_original.jpg"
section: "Salgados"
---

<article>
<h4>Ingredientes:</h4>

1. 1/2 pacote de macarrão penne ou parafuso;
2. 02 pepinos (pequenos);
3. 01 cenoura;
4. 100g de presunto fatiado;
5. 100g de queijo fatiado;
5. Maionese a gosto;
6. Azeite a gosto.
</article>

<article>
<h4>Modo de preparo:</h4>

1. Ferva 01 litro de água para cozinhar o macarrão, escorra e lave o macarrão;
2. Coloque o macarrão em uma vasilha com água gelada e gelo;
3. Fatie e desidrate os pepinos e a cenoura;
4. Escorra o macarrão;
5. Misture todos os ingredientes em uma travessa;
6. Tempere com azeite e maionese.
</article>