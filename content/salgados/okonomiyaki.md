---
title: "Okonomiyaki"
date: 2021-10-13T23:27:57-03:00
draft: false
photo: "https://www.justonecookbook.com/wp-content/uploads/2020/03/Hiroshima-OKonomiyaki-7692-I.jpg"
section: "Salgados"
---

<article>
<h4>Ingredientes para a massa (04 porções):</h4>

1. 01 xícara (chá) de farinha de trigo;
2. 01 xícara (chá) de água;
3. 01 ovo;
4. 01 colher (chá) de sal;
5. 01 inhame ou batata pequena (ralada).

<h4>Ingredientes para o recheio (04 porções):</h4>

1. Katsuobushi (peixe seco em lascas) a gosto;
2. 04 xícaras (chá) repolho fatiado fino;
3. 1/2 xícara (chá) cebolinha fatiada;
4. 200g bacon fatiado fino;
5. 250g macarrão cozido;
6. 04 ovos;
7. Molho de okonomiyaki a gosto;
8. Maionese a gosto.

<h4>Ingredientes para o molho (04 porções):</h4>

1. 01 xícara de molho Tonkatsu;
2. 1/2 xícara de ketchup;
3. 02 colheres (sopa) de molho inglês;
4. 01 colher (sopa) mostarda.
</article>

<article>
<h4>Modo de preparo:</h4>

1. Misture todos os ingredientes da massa em uma tijela e reserve;
2. Aqueça uma frigideira ou uma chapa de aço e despeje um pouco da massa em formato de círculo;
3. Em cima da massa coloque katsuobushi, repolho, cebolinha, o bacon, tempere com sal, acrescente um pouco da massa para dar liga, quando a massa soltar do fundo da frigideira, vire com uma espátula, aguarde uns minutos e depois aperte bem com a espátula para grelhar por completo.
Em outra frigideira, coloque um fio de óleo, grelhe o macarrão (~01 xícara), já cozido, com 02 colheres (sopa) do molho de okonomi, sobre este macarrão coloque o okonomiyaki.
Na mesma frigideira que preparou o okonomiyaki, frite 01 ovo, espalhando no fundo da frigideira, sobre o ovo, coloque o macarrão com okonomiyaki, pronto! Vire sobre um prato e decore com o molho de okonomi, a maionese e “ao-nori”.
</article>