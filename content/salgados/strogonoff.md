---
title: "Strogonoff"
date: 2021-10-13T23:27:57-03:00
draft: false
photo: "https://www.mundoboaforma.com.br/wp-content/uploads/2021/04/Strogonoff-frango.jpg"
section: "Salgados"
---

<article>
<h4>Ingredientes:</h4>

1. 500g alcatra ou filé mignon fatiados finos;
2. 02 colheres (sopa) manteiga;
3. 01 cebola (pequena);
4. 200g champignon;
5. 1/2 xícara (chá) de suco ou mollho de tomate;
5. 02 colheres (sopa) ketchup;
6. 01 colher (sopa) mostarda;
7. 01 colher (chá) molho inglês;
8. 01 caixa de creme de leite;
9. Sal a gosto.
</article>

<article>
<h4>Modo de preparo:</h4>

1. Derreta a manteiga e refogue a carne até dourar, quando estiver com a aparência de carne cozida, retire o caldo da carne e reserve;
2. Acrescente a cebola picada e frite com a carne até que ela murche;
3. Acrescente o ketchup, a mostarda, o molho inglês, o champignon fatiado e uma pitada de sal, e cozinhe um pouco;
4. Acrescente o molho de tomate e o caldo de carne (reservado);
5. Deixe ferver por uns 3 minutos;
6. Desligue o fogo, acrescente o creme de leite e misture bem.
</article>